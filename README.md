# Ku

The publish/subscribe pattern is used for loose coupling
between components. Messages that are published are not sent
to subscribers directly but through a message bus.

* Subscribers subscribe to messages using a regular expression routing key.
* Publishers publish to messages using a string routing key.

## Running Notes

1. Make sure elixir and git is install properly
2. ```git clone https://gitlab.com/bantl23/ku```
3. ```cd ku```
4. ```mix deps.get```, Gets dependencies.
5. ```mix compile```, Compiles code
6. ```mix credo```, Static analysis tool.
7. ```mix test```, Runs unit tests.
8. ```mix test --cover```, Runs code coverage
9. ```iex -S mix```, Runs application. Note: I created a mix
application, therefore when executing this line the application
start automatically. There is no need to execute Ku.start.
10. Execute examples from given requirements.

## Design

Publishers and Subscribers are linked via a messages bus. The message
bus needs to be highly available so it will be supervised by an OTP
Supervisor. The message bus data structure is an association table. The
table consists of "subscriber regular expression route keys" and "callbacks".
When a publisher publishes on the "publisher string route key" the table
is checked. If the "subscriber regular expression route key" matches the
"publisher string route key" the "callback" is called with the appropriate
message. Additionally when subscriber callbacks are executed each callback will
be executed in parallel using Task.async to achieve the desired parallel
features of Elixir.

## Assumptions

1. Publishers are don't care. If there are no subscribers on
a particular routing key no error is returned. Messages are
publish and forget.
2. Subscribers will only receive messages from the point at
which they connect. Storing messages is not a current feature.
3. Subscribers are implemented via function callbacks only. Blocking
functions are not a current feature.
4. Single message bus.

## Performance

Performance is crucial for publish/subscribe implementations as any
delay introduced by the message bus means the longer the wait times
for subscribers which could in turn introduce delays to end
users. Therefore parallelism needs to be employed where appropriate.
This will be handled using Task.async.

## High Availability

Leverage the Erlang's OTP framework to enable high
availability capabilities. Follow Erlang's philosophy of fail fast and
have supervisors restart failed components. Currently the system has
one supervisor which monitors the message bus. Subscribers will have to
reconnect on restart.

## Scaling Considerations

One method for scaling a publish/subscribe system would be
to add multiple messages buses. Subscribers will attached to
a particular messages bus using an algorithm. This creates the
need for an additional component which would route publish messages
to the appropriate message bus. The method for which subscribers
attaches to which message bus could be round-robin but a domain
specific algorithm would be a better solution. This way message
loads could be spread across the various messages buses which
could be hosted on different machines. In addition this creates
the need for more supervisors which will keep the system highly
available.

## Possible Enhancements

* Make the message bus association table persistent, so if a crash occurs
on the bus, state can be retrieved. Some care will have to be take because
in this situation there is a chance for a corrupted value to be persisted
causing repeated restarts.
* Add the ability to remove subscribers
* Add custom Logger backend to wrote data to a file for debugging

## GenStage

GenStage was considered as an implementation alternative but I found that it
was overkill for the scope of this task. In my opinion using GenServer with
an association table gets the job done understandably, simply and cleanly.
The negative to this implementation is back pressure (Jose Valim discusses
here: http://elixir-lang.org/blog/2016/07/14/announcing-genstage). The
publishers in this implementation send as fast as possible and if subscribers
can't keep up then lots of calls could be backed up which could
cause memory to be exhausted.

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed as:

  1. Add `ku` to your list of dependencies in `mix.exs`:

    ```elixir
    def deps do
      [{:ku, "~> 0.1.0"}]
    end
    ```

  2. Ensure `ku` is started before your application:

    ```elixir
    def application do
      [applications: [:ku]]
    end
    ```
