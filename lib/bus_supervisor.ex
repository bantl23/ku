defmodule BusSupervisor do
  @moduledoc """
  Bus supervisor monitors the message bus and restarts if it crashes.
  """

  use Supervisor

  @doc """
  Starts the supervisor task.
  """
  def start_link do
    Supervisor.start_link(__MODULE__, :ok, name: :bus_supervisor)
  end

  @doc """
  Starts supervising the message bus. If it goes down it follows
  the below recovery scheme.
  """
  def init(:ok) do
    children = [
      worker(Bus, [:bus])
    ]
    supervise(children, strategy: :one_for_one)
  end
end
