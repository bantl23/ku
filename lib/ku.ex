defmodule Ku do
  @moduledoc """
  The Ku publish/subscribe system interface.
  """

  require Logger

  @doc """
  Starts the publish/subscribe system using OTP supervisor. Started
  as an application so :observer.start can monitor it properly.
  """
  def start(_type, _arg) do
    BusSupervisor.start_link
  end

  @doc """
  Publishes a message and metadata to the system based on a route key.
  """
  def publish(route_key, message_body, metadata \\ %{}) when
      is_map(message_body) and is_map(metadata) do
    Logger.info("publishing to #{inspect route_key}")
    message = %Message{route: route_key, body: message_body, metadata: metadata}
    GenServer.cast(:bus, {:publish, message})
  end

  @doc """
  Subscribe to messages based on a regular expression key. If a message
  is received execute the callback.
  """
  def subscribe(route_key_regex, callback) when is_function(callback) do
    Logger.info("subscribing to #{inspect route_key_regex}")
    GenServer.call(:bus, {:add, route_key_regex, callback})
  end
end
