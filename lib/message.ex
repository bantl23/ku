defmodule Message do
  @moduledoc """
  Message format encapsulated as a structure.

  Keeping route because sometimes is useful to know the
  originating route key.
  """
  defstruct route: "", body: %{}, metadata: %{}
end
