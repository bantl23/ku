defmodule MyAnotherModule do
  @moduledoc """
  Example module used for subscriber callbacks
  """

  require Logger

  @doc """
  Logs the received message
  """
  def do_it(message) do
    Logger.info "#{inspect message.body}"
    Logger.info "#{inspect message.metadata}"
    {:ok}
  end

  @doc """
  Logs the received message
  """
  def also_do_it(message) do
    Logger.info "#{inspect message.body}"
    Logger.info "#{inspect message.metadata}"
    {:ok}
  end
end
