defmodule MyModule do
  @moduledoc """
  Example module used for subscriber callbacks
  """

  require Logger

  @doc """
  Logs the received message
  """
  def do_it(message) do
    Logger.info "#{inspect message.body}"
    Logger.info "#{inspect message.metadata}"
    {:ok}
  end
end
