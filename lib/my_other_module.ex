defmodule MyOtherModule do
  @moduledoc """
  Example module for subscriber callbacks
  """

  require Logger

  @doc """
  Logs the received message
  """
  def also_do_it(message) do
    Logger.info "#{inspect message.body}"
    Logger.info "#{inspect message.metadata}"
    {:ok}
  end
end
