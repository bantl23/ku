defmodule BusSupervisorTest do
  use ExUnit.Case
  doctest Ku

  test "supervisor started by default by application" do
    {status, {reason, pid}} = BusSupervisor.start_link
    assert status === :error
    assert reason === :already_started
    assert is_pid(pid)
  end

  test "init is ok and one_for_one" do
    {status, {{type, _, _}, _}} = BusSupervisor.init(:ok)
    assert status === :ok
    assert type == :one_for_one
  end
end
