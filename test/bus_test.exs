defmodule BusTest do
  use ExUnit.Case
  doctest Ku

  test "bus started by default by application" do
    {status, {reason, pid}} = Bus.start_link(:bus)
    assert status === :error
    assert reason === :already_started
    assert is_pid(pid)
  end

  test "empty init" do
    {status, table} = Bus.init
    assert status === :ok
    assert table === []
  end

  test "init with table" do
    input = [{~r/^foo$/, &IO.inspect/1}]
    {status, table} = Bus.init(input)
    assert status === :ok
    assert table === input
  end

  test "handle cast does not modify route table" do
    route_table = [{~r/^foo$/, &IO.inspect/1}]
    msg1 = %Message{route: "foo.bar", body: %{bar: "baz"}, metadata: %{optional: "metadata"}}

    {status, table} = Bus.handle_cast({:publish, msg1}, route_table)
    assert status === :noreply
    assert table === route_table

    msg2 = %Message{route: "hello", body: %{bar: "baz"}, metadata: %{optional: "metadata"}}
    {status, table} = Bus.handle_cast({:publish, msg2}, route_table)
    assert status === :noreply
    assert table === route_table
  end

  test "handle call updates route table" do
    {reply, status, table} = Bus.handle_call({:add, ~r/^foo$/, &IO.inspect/1}, :dont_care, [])
    assert reply === :reply
    assert status === :ok
    assert table === [{~r/^foo$/, &IO.inspect/1}]
  end
end
