defmodule EndToEndTest do
  use ExUnit.Case
  import Mock
  doctest Ku

  setup do
    Ku.start(:dontcare, :dontcare)
    Ku.subscribe ~r/^foo\.bar$/, &MyAnotherModule.do_it/1
    Ku.subscribe ~r/^foo\.*/, &MyAnotherModule.also_do_it/1
  end

  test "end to end foo.bar" do
    with_mock MyAnotherModule, [do_it: fn(_msg) -> {:ok} end, also_do_it: fn(_msg) -> {:ok} end] do
      msg = %Message{ route: "foo.bar", body: %{bar: "baz"}, metadata: %{optional: "metadata object"} }
      Ku.publish msg.route, msg.body, msg.metadata
      :timer.sleep(100)
      assert called(MyAnotherModule.do_it(msg)) === true
      assert called(MyAnotherModule.also_do_it(msg)) === true
    end
  end

  test "end to end foo.lala" do
    with_mock MyAnotherModule, [do_it: fn(_msg) -> {:ok} end, also_do_it: fn(_msg) -> {:ok} end] do
      msg = %Message{ route: "foo.lala", body: %{bar: "baz"}, metadata: %{optional: "metadata object"} }
      Ku.publish msg.route, msg.body, msg.metadata
      :timer.sleep(100)
      assert called(MyAnotherModule.do_it(msg)) === false
      assert called(MyAnotherModule.also_do_it(msg)) === true
    end
  end

  test "end to end unhandled_key" do
    with_mock MyAnotherModule, [do_it: fn(_msg) -> {:ok} end, also_do_it: fn(_msg) -> {:ok} end] do
      msg = %Message{ route: "unhandled_key", body: %{bar: "baz"}, metadata: %{optional: "metadata object"} }
      Ku.publish msg.route, msg.body, msg.metadata
      :timer.sleep(100)
      assert called(MyAnotherModule.do_it(msg)) === false
      assert called(MyAnotherModule.also_do_it(msg)) === false
    end
  end
end
