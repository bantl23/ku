defmodule KuTest do
  use ExUnit.Case
  doctest Ku

  test "supervisor started by default by application" do
    {status, {reason, pid}} = BusSupervisor.start_link
    assert status === :error
    assert reason === :already_started
    assert is_pid(pid)
  end

  test "bus started by default by application" do
    {status, {reason, pid}} = Bus.start_link(:bus)
    assert status === :error
    assert reason === :already_started
    assert is_pid(pid)
  end

  test "adding subscriber properly" do
    result = Ku.subscribe ~r/^foo\.bar$/, &IO.inspect/1
    assert result === :ok
  end

  test "adding subscriber improperly" do
    assert_raise FunctionClauseError, ~r/^no function clause matching in Ku.subscribe.*/, fn ->
      Ku.subscribe "hello", "hello"
    end
  end

  test "publishing properly" do
    result = Ku.publish "foo.bar", %{bar: "baz"}, %{optional: "metadata object"}
    assert result === :ok
  end

  test "publishing improperly" do
    assert_raise FunctionClauseError, ~r/^no function clause matching in Ku.publish.*/, fn ->
      Ku.publish "hello", "hello", %{}
    end

    assert_raise FunctionClauseError, ~r/^no function clause matching in Ku.publish.*/, fn ->
      Ku.publish "hello", %{}, "hello"
    end

    assert_raise FunctionClauseError, ~r/^no function clause matching in Ku.publish.*/, fn ->
      Ku.publish "hello", "hello", "hello"
    end
  end
end
