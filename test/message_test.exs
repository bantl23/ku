defmodule MessageTest do
  use ExUnit.Case
  doctest Message

  test "default initialization" do
    msg = %Message{}
    assert msg.route === ""
    assert msg.body === %{}
    assert msg.metadata === %{}
  end

  test "initialize with values" do
    route = "hello"
    body = %{baz: :foo}
    metadata = %{optional: :bar}

    msg = %Message{route: route, body: body, metadata: metadata}
    assert msg.route === route
    assert msg.body === body
    assert msg.metadata === metadata
  end
end
