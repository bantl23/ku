defmodule MyAnotherModuleTest do
  use ExUnit.Case
  doctest Ku

  test "make sure do_it doesn't crash" do
    {status} = MyAnotherModule.do_it(%Message{})
    assert status === :ok
  end

  test "make sure also_do_it doesn't crash" do
    {status} = MyAnotherModule.also_do_it(%Message{})
    assert status === :ok
  end
end
