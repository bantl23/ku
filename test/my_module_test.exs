defmodule MyModuleTest do
  use ExUnit.Case
  doctest Ku

  test "make sure do_it doesn't crash" do
    {status} = MyModule.do_it(%Message{})
    assert status === :ok
  end
end
